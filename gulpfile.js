const gulp = require("gulp");
const size = require("gulp-size");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const uglify = require("gulp-uglify");
const cssnano = require("cssnano");
const imagemin = require("gulp-imagemin");
const purgecss = require("@fullhuman/postcss-purgecss");

const safelist = ["header-scrolled", /^aos-/, /^data-aos-/, /^fade-/];

function cloneFiles(cb) {
  gulp
    .src("./src/**/*.{html,eot,svg,ttf,woff,woff2}")
    .pipe(gulp.dest("./build-example"));
  cb();
}

function processCSS(cb) {
  gulp
    .src("./src/**/*.css")
    .pipe(
      postcss([
        // autoprefixer(),
        cssnano({
          preset: require("cssnano-preset-advanced"),
          plugins: [["autoprefixer", { add: true }]],
        }),
        purgecss({
          content: ["./src/index.html"],
          safelist,
        }),
      ])
    )
    .pipe(gulp.dest("./build-example"));
  cb();
}

function processJS(cb) {
  gulp.src("./src/**/*.js").pipe(uglify()).pipe(gulp.dest("./build-example"));
  cb();
}

function processIMGS(cb) {
  gulp
    .src("./src/**/*.{png,jpg,jpeg}")
    .pipe(imagemin())
    .pipe(gulp.dest("./build-example"));
  cb();
}

exports.default = gulp.series(cloneFiles, processCSS, processJS, processIMGS);
