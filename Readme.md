# PostCSS - extending and controlling our css

![PostCSS](https://repository-images.githubusercontent.com/13078968/13690800-b56b-11ea-8a29-3d935ac52ce0)

- What's PostCSS and why to use it
- PostCSS vs SASS
- PurgeCSS (plugin)
- NanoCSS (plugin)
- Autoprefixer (plugin)


## Template used
Template Name: Squadfree
Template URL: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/
Author: BootstrapMade.com
License: https://bootstrapmade.com/license/

## What we'll be doing today

The starting point:
![starting point](./docs/start.png)

The end result:
![end result](./docs/end.png)


## The question - how to optimize our website via PostCSS
- Add PostCSS to process your code when building
- Use a tool like PurgeCSS to clear out the unnecessary things
- Add a compression tool like NanoCSS to make the available code as small as possible
- Add a prefixes tool like Autoprefixer to add support between browsers


## But I use Webpack .. 
- [How to configure PostCSS with webpack](https://blog.jakoblind.no/postcss-webpack/)
- [Webpack postcss-loader](https://webpack.js.org/loaders/postcss-loader/)